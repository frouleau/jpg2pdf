const fs = require('fs')
const path = require('path')
const PDFDocument = require('pdfkit')
const commandLineArgs = require('command-line-args')
const commandLineUsage = require('command-line-usage')

const optionDefinitions = [
  { name: 'help', alias: 'h', type: Boolean, description: 'Print this usage guide.' },
  { name: 'verbose', alias: 'v', type: Boolean, description: 'enable verbose mode.' },
  {
    name: 'in',
    alias: 'i',
    type: String,
    defaultValue: '.',
    defaultOption: true,
    typeLabel: '{underline folder}',
    description: 'The input folder containing the images. Default is current folder.'
  },
  {
    name: 'out',
    alias: 'o',
    type: String,
    defaultValue: 'output.pdf',
    typeLabel: '{underline output.pdf}',
    description: 'The output PDF document. default is output.pdf.'
  },
  { name: 'sort', alias: 's', type: Boolean, description: 'sort images by filename.' }
]

var options
try {
  options = commandLineArgs(optionDefinitions)
} catch (error) {
  console.error('Wrong parameters: ' + error)
  console.error('use --help')
  process.exit(-1)
}

if (options.help) {
  console.log(commandLineUsage([
    {
      header: 'jpg2pdf',
      content: [
        'Generates pdf document from jpeg/png images.',
        '(c) F.Rouleau {underline https://gitlab.com/frouleau/jpg2pdf}'
      ]
    },
    {
      header: 'Options',
      optionList: optionDefinitions
    }
  ]))
  process.exit(0)
}

const dir = options.in
fs.readdir(dir, function (err, items) {
  if (err) {
    console.error('Failed to open directory \'' + dir + '\'\n' + err)
    process.exit(-1)
  }
  var files = []
  for (var i = 0; i < items.length; i++) {
    const name = items[i]
    if (name.endsWith('.jpg') || name.endsWith('.jpeg') || name.endsWith('.png')) {
      files.push(name)
    }
  }
  if (options.sort) {
    files.sort()
  }
  generatePdf(files)
})

function generatePdf (files) {
  var doc = new PDFDocument({
    size: 'A4',
    layout: 'portrait'
  })
  const margin = 50
  const w = doc.page.width
  const h = doc.page.height
  doc.pipe(fs.createWriteStream(options.out))
  for (var i = 0; i < files.length; i++) {
    const name = files[i]
    if (i !== 0) {
      doc.addPage()
    }
    try {
      if (options.verbose) {
        console.log('Reading file ' + name)
      }
      const imgpath = path.join(dir, name)
      doc.image(imgpath, margin, margin, { fit: [w - 2 * margin, h - 2 * margin] })
      if (options.verbose) {
        console.log('File ' + name + ' added.')
      }
    } catch (error) {
      console.error('Failed to process file ' + name + '\n' + error)
    }
  }
  doc.end()
}
